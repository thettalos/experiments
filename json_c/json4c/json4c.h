#ifndef _JSON4C_H_
#define _JSON4C_H_

#define JSON_KEY_LEN	50

typedef char	NODE_KEY[JSON_KEY_LEN];
typedef char *	NODE_VALUE;
typedef void *	NODE_CHILDREN;

typedef struct JSON_NODE {
	NODE_KEY		node_key;
	NODE_VALUE		node_value;
	NODE_CHILDREN	children;
} JSON_NODE;

void json_node_init(JSON_NODE * root, NODE_KEY key, const NODE_VALUE value);
void json_node_add_element(JSON_NODE * root, NODE_KEY key, const NODE_VALUE value);
void json_node_destory(JSON_NODE * root);


#endif
