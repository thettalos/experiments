// json4c.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <tchar.h>
#include <memory.h>
#include "json4c.h"

typedef void * OBJECT_REF;

// you need a quick linked list to store hierarchical node children
typedef struct LINKED_NODE {
	OBJECT_REF				object_ref;
	struct LINKED_NODE *	next;
} LINKED_NODE;

typedef int(*link_node_enum_fn)(LINKED_NODE * current);

void linked_node_init(LINKED_NODE * head, OBJECT_REF object_ref) {
	// TODO:
}
void linked_node_append(LINKED_NODE * head, OBJECT_REF object_ref) {
	// TODO:
}
void linked_node_enum(LINKED_NODE * current, link_node_enum_fn enumFn) {
	// TODO:
}
void linked_node_destroy(LINKED_NODE * head) {
	// TODO:
}

static NODE_VALUE json_node_create_value(const NODE_VALUE value) {
	size_t valueLen = strlen(value);
	NODE_VALUE * newValue = malloc(valueLen);
}
void json_node_init(JSON_NODE * root, NODE_KEY key, const NODE_VALUE value) {
	memset(root, 0, sizeof(JSON_NODE));
	root->children = malloc(sizeof(LINKED_NODE));
	memset(root->children, 0, sizeof(LINKED_NODE));
}
void json_node_add_element(JSON_NODE * root, NODE_KEY key, const NODE_VALUE value) {

}
void json_node_destory(JSON_NODE * root) {

}

int main() {
	printf("hello world");
	getchar();

	return 0;
}

