/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['jquery'], function($) {
    var _eventCallbacks = {};
    var _hubElem = $('#event-hub');
    
    var broadcastEvent = function(cb,data) {
        setTimeout(function() {
           cb.apply(data); 
        },0);
    };
    
    var _eventHub = {
        subscribe: function(eventName, eventCallback) {
            if(!_eventCallbacks[eventName]) {
                _eventCallbacks[eventName] = [eventCallback];
                _hubElem.on(eventName, function(e,data) {
                   console.log(e); 
                   var callbackList = _eventCallbacks[eventName];
                   for(var ix = 0; ix < callbackList.length; ix ++) {
                        broadcastEvent(callbackList[ix],data);
                   }
                });
            } else {
                _eventCallbacks[eventName].push(eventCallback);
            }
        },
        publish: function(eventName, eventData) {
            _hubElem.trigger(eventName,[eventData]);
        }
    };
    return _eventHub;
});
