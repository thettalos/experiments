/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define([], function(can) {
    var _mapping = {
    };
    var _control = null;
    var _router = {
        navigateTo: function(localPath) {
            window.location.hash = "#!{0}".format(localPath);
        },
        currentPath: function() {
            var r = can.route.attr();
            return r;
        },
        registerRoute: function(path,callback,defaults) {
          _mapping["{0} route".format(path)] = function(data) {
                callback(data);  
          };
          can.route(path,defaults);
        },
        start: function() {
            can.route.ready(false);
            var RoutingControl = can.Control(_mapping);
            _control = new RoutingControl();
            can.route.ready(true);
        },
        ready: function() {
            return (_control !== null);
        }
    };
    return _router;
});