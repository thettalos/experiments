/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([], function() {
    var _utils = {
        loadTemplate : function(templateName, callback) {
            require(["text!./../templates/{0}.html".format(templateName)], function(c) {
               callback(c); 
            });
        },
        isFunction: function(f) {
            return typeof f === 'function';
        },
        runAsync: function(callback,delay) {
            var timeout = delay || 0;
            setTimeout(function() {
                callback();
            }, timeout);
        }
    };
    return _utils;
});
