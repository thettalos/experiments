/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define(['jquery', 'common/extensions'], function($) {
    var _clientSize = $("#client-size");
    function updateSize() {
        _clientSize.text("[{0}x{1}]".format(window.innerWidth, window.innerHeight));
    }
    $(window).resize(function(e) {
        updateSize();
    });
    $(document).ready(function() {
        updateSize();
    });
});