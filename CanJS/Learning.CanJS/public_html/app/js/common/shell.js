/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define(['jquery','common/eventhub', 'components/viewContainer', 'viewmodels/base', 
    'can.delegate'], 
    function($, eventHub, ViewContainer, viewModelBase) {
    var ShellViewModel = viewModelBase.extend({
        // activeView: "users"
        activeView : "dashboard"
    });
    var viewContainer = new ViewContainer('#view');
    var shellVM = new ShellViewModel();
    var activeViewChanged = function(ev, newVal, oldVal) {
        console.log(ev, newVal, oldVal);
        viewContainer.showView(shellVM.attr('activeView'), shellVM, function(viewName) {
            console.log("shell view '{0}' loaded.".format(viewName));
        });
    };
    var shell = {
        start: function() {
            console.log("shell started");
            eventHub.publish("shell-started");
            viewContainer.showView(shellVM.attr('activeView'), function(viewName) {
                console.log("shell view '{0}' loaded.".format(viewName));
                shellVM.delegate('activeView', 'set', activeViewChanged);
            });
        },
        setActiveView: function(viewName) {
            shellVM.attr('activeView', viewName);
        }
    };
    return shell;
});