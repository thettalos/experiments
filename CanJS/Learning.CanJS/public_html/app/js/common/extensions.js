/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define(['sprintf'], function() {
    if (!String.prototype.format) {
        String.prototype.format = function() {
            var formatted = this;
            for (var arg in arguments) {
                formatted = formatted.replace("{" + arg + "}", arguments[arg]);
            }
            return formatted;
        };
    }
    if(!String.prototype.printf) {
        String.prototype.printf = function(format) {
            var formatted = this;
            formatted = sprintf(format,formatted);
            return formatted;
        };
    }
    if(!Number.prototype.printf) {
        Number.prototype.printf = function(format) {
            var formatted = this;
            formatted = sprintf(format,formatted);
            return formatted;
        };
    }
});