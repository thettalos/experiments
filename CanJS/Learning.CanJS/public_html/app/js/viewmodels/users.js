/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define(['viewmodels/viewModelFactory'], function(viewModelFactory) {
    var config = {
        resource: {
          name: 'users'  
        },
        fields: {
            updatable: ['Username', 'password'],
            viewable: [
                viewModelFactory.createFieldSorting('_id', 'User Id', true),
                viewModelFactory.createFieldSorting('Username', 'User Name'),
                viewModelFactory.createFieldSorting('password', 'Password'),
                viewModelFactory.createFieldSorting('_created', 'Date Created'),
                viewModelFactory.createFieldSorting('_updated', 'Date Updated')
            ]}
    };
    //NOTE: next goal is to read this configuration from app/config/resources
    var viewModel = viewModelFactory.createResourceViewModel(config).extend({});
    return viewModel;
});
