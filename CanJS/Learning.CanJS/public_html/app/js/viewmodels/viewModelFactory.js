/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['jquery','underscore', 'can', 'common/utils', 
    'viewmodels/base', 'viewmodels/paging',
    'common/eventhub', 'common/config',
    'can.delegate', 'components/grid'],
        function($, _, can, utils, ViewModelBase, PagingViewModel, eventHub, conf) {
            var baseUrl = conf.services.main.baseUrl;
            var _factory = {
                createViewModel: function(proto) {
                  return ViewModelBase.extend(proto);  
                },
                createResourceViewModel: function(config) {
                    var resourceName = config.resource.name;
                    var baseResourceUrl = "{0}/{1}".format(baseUrl, resourceName);
                    var ResourceModel = can.Model.extend({
                        id: "_id",
                        findAll: 'GET {0}?max_results={pageSize}&page={page}&sort=[("{sorting.field}",{sorting.order})]'.format(baseResourceUrl),
                        findOne: 'GET {0}/{_id}'.format(baseResourceUrl),
                        create: 'POST {0}'.format(baseResourceUrl),
                        // update: 'PATCH {0}/{_id}'.format(baseResourceUrl),
                        update: function(id, attrs) {
                            var df = new can.Deferred();
                            var headers = {};
                            var data = {};
                            for(var k in config.fields.updatable) {
                                var name = config.fields.updatable[k];
                                data[name] = attrs[name];
                            }
                            // headers['If-Match'] = attrs._etag;
                            return $.ajax({
                                url: '{0}/{1}'.format(baseResourceUrl,id),
                                method: 'PATCH',
                                data: data,
                                type: 'json',
                                crossDomain: true,
                                headers: headers,
                                success: function(resp) {
                                    console.log(resp);
                                    if(resp._status !== 'ERR') {
                                        df.resolve(resp);
                                    } else {
                                        df.reject(resp);
                                    }
                                },
                                error: function(err) {
                                    console.log(err);
                                    df.reject(err);
                                }
                            });
                            return df;
                        },
                        destroy: 'DELETE {0}/{_id}'.format(baseResourceUrl),
                        parseModels: function(data) {
                            return data._items;
                        },
                        resourceName: resourceName
                    }, {});

                    var ViewModel = ViewModelBase.extend({
                        paging: new PagingViewModel(),
                        pagedItems: [],
                        fields: config.fields.viewable,
                        sorting: null,
                        fetching: false,
                        getSelectedRow: function(rowId) {
                            var that = this;
                            var fields = that.attr('fields');
                            var keyField = _.findWhere(fields, {isKey: true});
                            if (keyField) {
                                var params = {};
                                params[keyField.name] = rowId;
                                var row = _.findWhere(that.attr('pagedItems'), params);
                                return row;
                            }
                            return null;
                        },
                        fetchCurrentPage: function(newSorting) {
                            var that = this;
                            var page = that.attr("paging.currentPage");
                            var pageSize = that.attr("paging.pageSize");
                            if (newSorting) {
                                that.attr('sorting', newSorting);
                            }
                            var sorting = that.attr('sorting');
                            if (!sorting) {
                                var fields = that.attr('fields');
                                that.attr('sorting', {
                                    field: fields[0].name,
                                    order: 1
                                });
                                sorting = that.attr('sorting');
                            }
                            that.attr('fetching', true);
                            var params = {
                                page: page,
                                pageSize: pageSize,
                                sorting: {
                                    field: sorting.attr('field'),
                                    order: sorting.attr('order')
                                }};
                            ResourceModel.findAll(params, function(data, p1, resp) {
                                console.log("data arrived for resource '{0}'".format(ResourceModel.resourceName));
                                var meta = resp.responseJSON._meta;
                                var modelUpdated = function(ev) {
                                    var model = ev.target;
                                    console.log("updated -->" + model._id);
                                    model.unbind("updated", modelUpdated);
                                    that.fetchCurrentPage(); 
                                };
                                var items = _.map(data, function(i) {
                                    var r = i;
                                    r.bind("updated", modelUpdated);
                                    return r;
                                });
                                that.attr("pagedItems", items);
                                that.attr("paging.total", meta.total);
                                eventHub.publish('{0}:page-arrived'.format(resourceName));
                                that.attr('fetching', false);
                                if (that.dataArrived) {
                                    that.dataArrived(items);
                                }
                            }, function(err) {
                                var msg = "{0} --> {1}".format(err.status, err.statusText);
                                console.log(msg);
                            });
                        },
                        sortCurrentPage: function(fieldHeader) {
                            var that = this;
                            var isFetching = that.attr('fetching');
                            if (!isFetching) {
                                var data = that.attr('pagedItems');
                                var fields = that.attr('fields');
                                var col = _.findWhere(fields, {header: fieldHeader});
                                if (col) {
                                    var sorting = col.attr('sorting');
                                    if (!sorting) {
                                        col.attr('sorting', {asc: true});
                                        sorting = col.attr('sorting')
                                    }
                                    if (sorting.attr('asc')) {
                                        sorting.attr('asc', false);
                                        that.fetchCurrentPage({field: col.name, order: -1});
                                    } else {
                                        sorting.attr('asc', true);
                                        that.fetchCurrentPage({field: col.name, order: 1});
                                    }
                                }
                            }
                        },
                        init: function() {
                            var that = this;
                            that.fetchCurrentPage();
                            that.delegate('paging.currentPage', 'set', function() {
                                that.fetchCurrentPage();
                            });
                            that.delegate('paging.pageSize', 'set', function() {
                                that.fetchCurrentPage();
                            });
                        }
                    });
                    return ViewModel;
                },
                createFieldSorting: function(name, header, isKey) {
                    return {
                        name: name,
                        header: header,
                        sorting: null,
                        isKey: isKey || false,
                        hasSorting: function() {
                            return this.attr('sorting') != null;
                        }
                    }
                }
            };
            return _factory;
        });
