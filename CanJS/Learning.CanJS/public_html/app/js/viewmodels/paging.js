/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define(['viewmodels/base'], function(ViewModelBase) {
    var PagingViewModel = ViewModelBase.extend({
        currentPage: 1,
        pageSize: 5,
        total: 0,
        allowedSizes: [1,5,10,20,50,100],
        totalPages : function() {
            var that = this;
            var pageSize = parseInt( that.attr('pageSize') );
            var total = parseInt( that.attr('total') );
            if(total) {
                var r = Math.ceil(total/ pageSize );
                return r;
            } else {
                return 0;
            }
        },
        canFetchNext: function() {
            var that = this;
            var pageSize = that.attr('pageSize');
            var page = that.attr('currentPage');
            return ((page) * pageSize) <= that.attr('total');
        },
        fetchNext: function() {
            console.log();
            var pg = this.attr('currentPage');
            this.attr('currentPage', pg + 1);
        },
        fetchLast: function() {
            console.log();
            var pg = this.attr('currentPage');
            this.attr('currentPage', this.totalPages());
        },
        canFetchPrev: function() {
            var that = this;
            var page = that.attr('currentPage');
            return (page > 1);
        },
        fetchPrev: function() {
            console.log();
            var pg = this.attr('currentPage');
            this.attr('currentPage', pg - 1);
        },
        fetchFirst: function() {
            console.log();
            this.attr('currentPage', 1);
        }
    });
    return PagingViewModel;
});
