/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define(['viewmodels/viewModelFactory'], function(viewModelFactory) {
    var defaultContent = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
    var viewModel = viewModelFactory.createViewModel({
        widgets : [
            { header: "Widget 1", template : "views/widgets/default", content: defaultContent },
            { header: "Widget 2", template : "views/widgets/default", content: defaultContent  },
            { header: "Widget 3", template : "views/widgets/default", content: defaultContent  },
            { header: "Widget 4", template : "views/widgets/default", content: defaultContent  },
            { header: "Widget 5", template : "views/widgets/default", content: defaultContent  },
            { header: "Widget 6", template : "views/widgets/default", content: defaultContent  },
            { header: "Widget 7", template : "views/widgets/default", content: defaultContent  },
            { header: "Widget 8", template : "views/widgets/default", content: defaultContent  },
            { header: "Widget 9", template : "views/widgets/default", content: defaultContent  }
        ]
    });
    return viewModel;
});
