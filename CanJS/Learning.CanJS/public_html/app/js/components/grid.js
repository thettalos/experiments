/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['jquery', 'can',
    'text!./../../templates/components/grid/paging.html',
    'text!./../../templates/components/grid/grid.html'], 
function($, can, pagingTemplate, gridTemplate) {
    
    can.Component.extend({
        tag: 'paging',
        template: pagingTemplate
    });
    
    can.Component.extend({
        tag: 'grid',
        template: gridTemplate,
        scope: {
          selectedRow: null,
          editTemplate: "@"
        },
        events: {
            'th.normal-column click' : function(th, event) {
                var that = this;
                var dataSource = that.scope.attr('source');
                dataSource.sortCurrentPage( th.find('.header').text() );
            },
            'tbody tr click': function(tr, event) {
                console.log();
                tr.parent().find("tr").removeClass("selected");
                tr.addClass("selected");
                //NOTE: assume that table header contains the row id field has been tagged with 'row-id' css class
                var rowId = tr.find("td.row-id").text();
                var that = this;
                var dataSource = that.scope.attr('source');
                var row = dataSource.getSelectedRow(rowId);
                that.scope.attr('selectedRow', row);
            }
        }
    });
});
