/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define(['jquery', 'can', 'common/utils'], function($, can, utils) {
    can.mustache.registerHelper("rowIndex", function(pageSizeEval, currentPageEval, itemIndexEval, options) {
        var pageSize = pageSizeEval();
        var page = currentPageEval() - 1;
        var itemIndex = itemIndexEval();
        var rowIndex = (pageSize * page) + itemIndex + 1;
        return rowIndex.printf('%02d');
    });
    can.mustache.registerHelper("rowToArray", function(itemAccessor, fieldsAccessor, options) {
        var output = [];
        var item = utils.isFunction(itemAccessor) ? itemAccessor() : itemAccessor;
        var fields = utils.isFunction(fieldsAccessor) ? fieldsAccessor() : fieldsAccessor;
        for (var ix = 0; ix < fields.length; ix++) {
            var f = fields[ix];
            var v = item[f.name];
            if (typeof v !== 'function') {
                var t = options.fn({name: f.name, value: v, isKey: f.isKey});
                output.push(t.trim());
            }
        }
        var r = output.join("\n");
        return r;
    });
    can.mustache.registerHelper("renderSelectedRow", function(templateEval, options) {
        return function(elem) {
            var viewModel = options.context;
            var templateId = templateEval();
            function updateTemplate(rowModel) {
                utils.loadTemplate(templateId, function(template) {
                    var view = can.mustache(template);
                    var content = view(rowModel);
                    $(elem).html(content);
                });
            };
            viewModel.delegate('selectedRow', 'set', function() {
                updateTemplate(viewModel.attr('selectedRow'));
            });
            updateTemplate(viewModel.attr('selectedRow'));
        };
    });
    can.mustache.registerHelper("widget", function(widget) {
        var templateId = widget.template;
        return function(el) {
            utils.loadTemplate(templateId, function(template) {
                var view = can.mustache(template);
                var content = view(widget);
                $(el).html(content);
            });
        };
    });
    can.mustache.registerHelper("test", function(arg1, arg2, arg3, arg4) {
        console.log();
        var r1 = arg1();
    });
});