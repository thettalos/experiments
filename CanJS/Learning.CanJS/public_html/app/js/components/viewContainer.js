/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(['jquery', 'can', 'common/utils', 
    'text!./../../templates/components/view-container.html',
    'components/helpers',
    'jquery.metadata', 'jquery.table-sort'],
        function($, can, utils, containerTemplate) {

            var ViewContainer = function(containerId) {
                var that = this;
                var ready = false;
                that.showView = function(viewName, viewLoaded) {
                    if (!ready) {
                        var containerView = can.mustache(containerTemplate);
                        $(containerId).html(containerView({}));
                        ready = true;
                    }
                    utils.loadTemplate('views/{0}/index'.format(viewName), function(viewTemplate) {
                        var rendering = can.mustache(viewTemplate);
                        require(['viewmodels/{0}'.format(viewName)], function(ViewModel) {
                            var content = rendering(new ViewModel());
                            $(containerId).find(".view-content").html(content);
                            if (viewLoaded) {
                                viewLoaded(viewName);
                            }
                        });
                    });
                };
                return that;
            };
            return ViewContainer;
        });

