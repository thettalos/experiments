/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// For any third party dependencies, like jQuery, place them in the lib folder.

// Configure loading modules from the lib directory,
// except for 'app' ones, which are in a sibling
// directory.
console.log();

requirejs.config({
    baseUrl: 'js',
    paths: {
        'sprintf' : '../lib/utils/sprintf',
        'underscore' : '../../bower_components/underscore/underscore',
        'jquery' : '../../bower_components/jquery/dist/jquery',
        'jquery.cookie' : '../../bower_components/jquery.cookie/jquery.cookie',
        'jquery.placeholder' : '../../bower_components/jquery-placeholder/jquery.placeholder',
        'jquery.metadata' : '../lib/plugins/jquery.metadata',
        'jquery.table-sort' : '../lib/plugins/jquery.table-sort',
        'iscroll' : '../lib/plugins/iscroll',
        'can' : '../../bower_components/canjs/amd/can',
        'can.delegate' : '../../bower_components/canjs/amd/can/map/delegate',
        'text' : '../../bower_components/requirejs-text/text',
        'groundwork' : '../../bower_components/groundwork/js/groundwork.all',
        'bootstrap' : '../../bower_components/bootstrap/dist/js/bootstrap'
    },
    shim: {
        'underscore': { 'exports': '_' },
        'jquery.cookie': { 'deps': ['jquery'] },
        'jquery.placeholder': { 'deps': ['jquery'] },
        'jquery.metadata': { 'deps': ['jquery'] },
        'jquery.table-sort': { 'deps': ['jquery'] },
        'groundwork': { 'deps': ['modernizr','jquery'] },
        'bootstrap': { 'deps': ['jquery'], exports: 'IScroll' },
    }
});

requirejs(['start']);