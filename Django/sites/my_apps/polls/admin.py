from django.contrib import admin
from polls.models import Poll, Choice

# Register your models here.
class PollAdmin_V1(admin.ModelAdmin):
    fields = ['pub_date', 'question']

class PollAdmin_V2(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question']}),
        ('Date information', {'fields': ['pub_date']}),
    ]

class PollAdmin_V3(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]

class ChoiceInline_V1(admin.StackedInline):
    model = Choice
    extra = 3

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3

class PollAdmin(admin.ModelAdmin):
    list_display = ('question', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question']
    fieldsets = [
        (None,               {'fields': ['question']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]


admin.site.register(Poll,PollAdmin)
admin.site.register(Choice)
