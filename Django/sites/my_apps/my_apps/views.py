'''
Created on Jul 17, 2014

@author: freesrc
'''
from django.views import generic
from django.http.response import HttpResponse
from django.shortcuts import render

def menu_vm():
    return {
        'header': 'Team Diary',
        'moto': 'Make team decisions easier',
        'options': [
            {
                'title': 'Conferences',
                'link' : 'teamdiary:index'
            },
            {
                'title': 'Participations',
                'link' : 'teamdiary:index'
            },
            {
                'title': 'Locations',
                'link' : 'teamdiary:index'
            },
        ]
    }
    
def view_vm():
    return {
        
    }


def home(request):
    context = { 'menu': menu_vm(), 'view' : view_vm() }
    return render(request, 'index.html', context)    
    