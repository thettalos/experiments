from django.contrib import admin
from teamdiary.models import Location, Participation, Conference,\
    Proposal, Argument, Voting, Discussion, ExpectedOutcome

# Register your models here.

admin.site.register(Location)

#+++++++++++++++++++++++++++++++++++++++++++++++++
class ParticipationAdmin(admin.ModelAdmin):
    list_display = ('conference', 'user', 'opinion_factor')
    
admin.site.register(Participation, ParticipationAdmin)

#+++++++++++++++++++++++++++++++++++++++++++++++++
class ExpectedOutcomeAdmin(admin.ModelAdmin):
    list_display = ['title', 'discussion']
    
admin.site.register(ExpectedOutcome, ExpectedOutcomeAdmin)

#+++++++++++++++++++++++++++++++++++++++++++++++++
class OutcomeInline(admin.TabularInline):
    model = ExpectedOutcome
    extra = 2

class DiscussionAdmin (admin.ModelAdmin):
    inlines = [OutcomeInline]
        
admin.site.register(Discussion,DiscussionAdmin)

#+++++++++++++++++++++++++++++++++++++++++++++++++
class ConferenceAdmin (admin.ModelAdmin):
    list_display = ['title', 'discussion', 'location', 'start_date', 'end_date']
    
admin.site.register(Conference, ConferenceAdmin)

#+++++++++++++++++++++++++++++++++++++++++++++++++
class ArgumentInline(admin.StackedInline):
    model = Argument
    extra = 1

class ProposalAdmin (admin.ModelAdmin):
    list_display=['title', 'submitted_by','total_weight']
    inlines = [ArgumentInline]

admin.site.register(Proposal, ProposalAdmin)

#+++++++++++++++++++++++++++++++++++++++++++++++++
class ArgumentAdmin (admin.ModelAdmin):
    list_display = ['proposal', 'weight']

admin.site.register(Argument, ArgumentAdmin)

#+++++++++++++++++++++++++++++++++++++++++++++++++
class VotingAdmin (admin.ModelAdmin):
    list_display = ['participation', 'argument', 'votes']

admin.site.register(Voting, VotingAdmin)



