'''
Created on Jul 17, 2014

@author: freesrc
'''
from django.contrib.auth.models import User
from teamdiary.models import Location

def create_users(prefix,start_from = 1, how_many = 5):
    for i in range(start_from,how_many):
        u = User(username='%s%02d' % (prefix, i), password="111111")
        u.save()

def create_developers():
    create_users('developer')
    
def create_managers():
    create_users('manager')

def create_directors():
    create_users('director')
        
def create_default_users():
    create_developers()
    create_managers()
    create_directors()

def create_default_locations():
    l1 = Location(address='Company Building - Room 55', type = 1)
    l1.save()
    # l2 = Location(address='Company Site - Forum', type = 2)
    # l2.save()
    return l1
    

def setup_database():
    create_default_users()
    create_default_locations()
    