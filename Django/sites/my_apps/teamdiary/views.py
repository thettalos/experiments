from django.shortcuts import render
from django.views import generic
from django.utils import timezone
from teamdiary.models import Conference

# Create your views here.

class IndexView(generic.ListView):
    template_name = 'teamdiary/conference/index.html'
    context_object_name = 'last_conferences'

    def get_queryset(self):
        r = Conference.objects.order_by('-date_created')[:5]
        return r

class DetailView(generic.ListView):
    template_name = 'teamdiary/conference/details.html'
