from django.db import models
from django.contrib.auth.models import User

# Create your models here.

#+++++++++++++++++++++++++++++++++++++++++++++++++
class ModelBase (models.Model):
    class Meta:
        abstract = True
        
    date_created = models.DateTimeField(auto_now=True)
    date_updated = models.DateTimeField(auto_now=True)
#+++++++++++++++++++++++++++++++++++++++++++++++++
class Location (ModelBase):
    LOCATION_TYPES = (
        (1, 'Meeting Room'),
        (2, 'Forum'),
    )
    address = models.CharField(max_length=255, unique=True)
    type = models.IntegerField(choices=LOCATION_TYPES)
    def __unicode__(self):
        return self.address
#+++++++++++++++++++++++++++++++++++++++++++++++++
class Participation (ModelBase):
    OPINION_FACTORS = (
        (1, 'Default'),
        (2, 'Aware'),
        (3, 'Experienced'),
        (5, 'Accountable'),
        (7, 'Decision Maker'),
    )
    user = models.ForeignKey(User)
    conference = models.ForeignKey('teamdiary.Conference')
    opinion_factor = models.IntegerField(choices=OPINION_FACTORS)
    class Meta:
        unique_together = ('user','conference')
    def __unicode__(self):
        return unicode(self.user)
#+++++++++++++++++++++++++++++++++++++++++++++++++
class Discussion (ModelBase):
    DISCUSSION_TYPES = (
        (1, 'Debate'),
        (2, 'Product Strategy'),
        (4, 'Technology Selection'),
        (8, 'Colleagues Dispute'),
    )
    title = models.CharField(max_length=255, unique=True)
    description = models.TextField()
    type = models.IntegerField(choices=DISCUSSION_TYPES)
    def __unicode__(self):
        return self.title
#+++++++++++++++++++++++++++++++++++++++++++++++++
class ExpectedOutcome (ModelBase):
    discussion = models.ForeignKey(Discussion)
    title = models.CharField(max_length=255)
    class Meta:
        unique_together = ('discussion','title')
    def __unicode__(self):
        return self.title
#+++++++++++++++++++++++++++++++++++++++++++++++++
class Conference (ModelBase):
    title = models.CharField(max_length=255)
    discussion = models.ForeignKey(Discussion)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    location = models.ForeignKey(Location)
    def __unicode__(self):
        return self.title
#+++++++++++++++++++++++++++++++++++++++++++++++++
class Proposal (ModelBase):
    title = models.CharField(max_length=255, unique=True)
    submitted_by = models.ForeignKey(Participation, unique=True)
    def total_weight(self):
        total = 0
        args = self.argument_set.all()
        for vt in args:
            total += vt.weight()
        return total
    def __unicode__(self):
        return self.title
#+++++++++++++++++++++++++++++++++++++++++++++++++
class Argument(ModelBase):
    proposal = models.ForeignKey(Proposal)
    justification = models.TextField()
    def weight(self):
        total = 0
        arg_votes = self.voting_set.all()
        for vt in arg_votes:
            total += vt.weight()
        return total
    def __unicode__(self):
        return "Argument for %s" % self.proposal
#+++++++++++++++++++++++++++++++++++++++++++++++++
class Voting(ModelBase):
    VOTE_TYPES = (
        (5, 'I Consent'),
        (10, 'I Agree'),
        (20, 'I Totally Agree'),
    )
    participation = models.ForeignKey(Participation)
    argument = models.ForeignKey(Argument)
    votes = models.IntegerField(choices=VOTE_TYPES)
    class Meta:
        unique_together = ('participation','argument', 'votes')
        
    def weight(self):
        return self.votes * self.participation.opinion_factor
    def __unicode__(self):
        return "Vote for %s" % self.argument
